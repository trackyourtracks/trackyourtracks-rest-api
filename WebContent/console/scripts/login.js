// LOGIN.JS

var TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0cmFja3lvdXJ0cmFja3MifQ.M6iIh58pjXUX4HrJvkfbI3iUyuNx8VKLy8MQCyQQVDA";

$(function ()
{

    // LOGIN

    ButtonToggle.init($("#admin_login_button"), [0, 1]);

    $("#admin_login_button").click(function ()
    {
        loginAdmin(
        {
            'user_name': $("#admin_username").val(),
            'user_password': $("#admin_password").val()
        });
    });

});

function loginAdmin(user)
{
    var login_alert = $("#admin_login_alert");
    $.ajax(
        {
            method: "GET",
            url: "https://trackyourtracks.eu-gb.mybluemix.net/api/communities/trackyourtracks",
            beforeSend: function (request)
            {
                request.setRequestHeader("Token", TOKEN);
                request.setRequestHeader("Authorization", "Basic " + btoa(user.user_name + ":" + user.user_password));
            }

        }).done(function (data)
        {

            if (login_alert.is(':visible'))
            {
                login_alert.hide();
            }

            window.sessionStorage.setItem("TrackYourTracks-Admin", JSON.stringify(user));

            window.location = "invoices.html";

        })
        .fail(function ()
        {
            if (!login_alert.is(':visible'))
            {
                login_alert.show();
            }
        });
}
