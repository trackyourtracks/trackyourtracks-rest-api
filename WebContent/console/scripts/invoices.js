// INVOICES.JS

var COMS = [
    {
        "com": "tindbike",
        "mail": "daniel.hofmeister@haw-hamburg.de"
    },
    {
        "com": "hawai_cycle",
        "mail": "sebastian.cordes@haw-hamburg.de"
    },

    {
        "com": "trackyourtracks",
        "mail": "timo.feddersen@haw-hamburg.de"
    }
];

$(function ()
{

    generateInfo();
    generateInvoiceInfo();

    $("#admin_logout_button").click(function ()
    {
        window.sessionStorage.clear();
        window.location = "login.html";
    });

    loadCommunities();

    $("#send_all").click(function ()
    {
        for (var i = 0; i < COMS.length; i++)
        {
            sendInvoice(COMS[i]).call();
        }
    });

});


function generateInfo()
{
    $("#admin_info").html("Logged in as <strong> TrackYourTracks Admin</strong>. (<u id='admin_logout_button'>Logout?</u>)");
}

function generateInvoiceInfo()
{
    var now = new Date();
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    $("#invoice_info").html("Invoices (" + monthNames[now.getMonth()] + ", " + now.getFullYear() + ")");
}

function loadCommunities()
{
    var invoices = $("#invoices");
    invoices.html("");

    for (var i = 0; i < COMS.length; i++)
    {
        var com_string = buildCommunity(i, COMS[i]);
        invoices.append(com_string);
        $("#send_" + i).click(sendInvoice(COMS[i]));
    }

    for (var j = 0; j < COMS.length; j++)
    {
        loadInvoice(j, COMS[j]);
    }
}

function buildCommunity(id, community)
{

    var s = "<strong class='tyt-blue' style='font-size: 14px; line-height: 24px;'>";
    var e = "</strong>";

    var com_string = "<li class='list-group-item'>" +
        "<h4 class='tyt-blue'><strong>" + community.com + "</strong></h4>" +
        s + "Mail: " + e + " <a href='mailto:" + community.mail + "'>" + community.mail + "</a>" +
        "<p id='com_" + id + "'>" + "</p>" +
        "<a id='send_" + id + "' type='button' class='btn btn-default tyt-button'>Send</a>" +
        "</li>";

    return com_string;
}

function loadInvoice(id, community)
{
    $.ajax(
    {
        method: "GET",
        url: "https://trackyourtracks.eu-gb.mybluemix.net/api/invoice",
        beforeSend: function (request)
        {
            request.setRequestHeader("com", community.com);
        }
    }).done(function (data)
    {
        var invoice_string = buildInvoice(id, data);
        $("#com_" + id).html(invoice_string);
    });
}

function buildInvoice(id, data)
{

    var s = "<strong class='tyt-blue' style='font-size: 14px; line-height: 24px;'>";
    var e = "</strong>";

    var invoice_string = "" +
        s + "Date: " + e + nextMonthString() + "<br>" +
        s + "User Count: " + e + data.count + "<br>" +
        s + "Amount: " + e + "<br>" +
        s + " > " + e + data.netto + " &euro; (net)" + "<br>" +
        s + " > " + e + data.brutto + " &euro; (gross)";

    return invoice_string;
}

function nextMonthString()
{
    var options = {
        weekday: 'long',
        year: 'numeric',
        month: 'long',
        day: 'numeric'
    };

    return nextMonth().toLocaleString('en-US', options);
}

function nextMonth()
{
    var now = new Date();
    if (now.getMonth() == 11)
    {
        return new Date(now.getFullYear() + 1, 0, 1);
    }
    else
    {
        return new Date(now.getFullYear(), now.getMonth() + 1, 1);
    }
}

function sendInvoice(community)
{
    return function ()
    {
        $.ajax(
        {
            method: "POST",
            url: "https://trackyourtracks.eu-gb.mybluemix.net/api/invoice",
            beforeSend: function (request)
            {
                request.setRequestHeader("com", community.com);
                request.setRequestHeader("mail", community.mail);
            }
        });
    };
}
