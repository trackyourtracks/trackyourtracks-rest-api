// INVOICES-CHECK.JS

if (window.sessionStorage.getItem("TrackYourTracks-Admin") === null)
{
    window.location = "login.html";
}
else
{
    var TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0cmFja3lvdXJ0cmFja3MifQ.M6iIh58pjXUX4HrJvkfbI3iUyuNx8VKLy8MQCyQQVDA";
    var admin = JSON.parse(window.sessionStorage.getItem("TrackYourTracks-Admin"));
    $.ajax(
        {
            method: "GET",
            url: "https://trackyourtracks.eu-gb.mybluemix.net/api/communities/trackyourtracks",
            beforeSend: function (request)
            {
                request.setRequestHeader("Token", TOKEN);
                request.setRequestHeader("Authorization", "Basic " + btoa(admin.user_name + ":" + admin.user_password));
            }
        })
        .fail(function ()
        {
            window.location = "login.html";
        });
}
