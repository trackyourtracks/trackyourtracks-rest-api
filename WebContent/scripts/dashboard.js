// DASHBOARD.JS

var TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0cmFja3lvdXJ0cmFja3MifQ.M6iIh58pjXUX4HrJvkfbI3iUyuNx8VKLy8MQCyQQVDA";

var user = Storage.getUser();

var new_track_map, new_track_directions_renderer, new_track_directions_service;

$(function ()
{

    generateAvatar();
    generateGreeting();
    generateInfo();

    $("#logout_button").click(function ()
    {
        Storage.clear();
        window.location = "welcome.html";
    });

    // MY TRACKS

    $("#my_tracks_heading").click(function ()
    {
        $("#my_tracks_heading_top").toggle();
        $("#my_tracks_heading_bottom").toggle();
        $("#my_tracks_body").toggle();
    });

    getMyTracks(user);

    // NEW TRACK

    var new_track_map_init_flag = false;
    $("#new_track_heading").click(function ()
    {
        $("#new_track_heading_top").toggle();
        $("#new_track_heading_bottom").toggle();
        $("#new_track_body").toggle();
        if (!new_track_map_init_flag)
        {
            initNewTrackMap();
            new_track_map_init_flag = true;
        }
    });

    ButtonToggle.init($("#new_track_button"), [0]);

    $("#new_track_button").click(function ()
    {
        createNewTrack(
        {
            'track_name': $("#new_track_title").val(),
            'track_description': $("#new_track_description").val(),
            'track_keywords': $("#new_track_keywords").val().split(","),
            'track_geojson': MapUtils.exportDirections(new_track_directions_renderer)
        });
    });

});

function generateAvatar()
{
    var random_number = Math.floor((Math.random() * 8) + 1);
    $("#user_avatar").attr("src", "images/avatars/a" + random_number + ".png");
}

function generateGreeting()
{
    var greetings = [
        "Hey <user>!<br>Enjoy your day.",
        "Yo <user>!<br>Take it easy.",
        "Hello <user>!<br>You are awesome.",
        "Yo <user>!<br>What's up?",
        "Hey <user>!<br>Nice to see you!",
        "Hey <user>!<br>How're you doin'?"
    ];
    var random_number = Math.floor((Math.random() * greetings.length));
    var greeting = greetings[random_number].replace("<user>", user.user_name);
    $("#user_greeting").html(greeting);
}

function generateInfo()
{
    $("#user_info").html("Logged in as <strong>" + user.user_mail + "</strong>. (<u id='logout_button'>Logout?</u>)");
}

function getMyTracks()
{
    $.ajax(
    {
        method: "GET",
        url: "https://trackyourtracks.eu-gb.mybluemix.net/api/users/" + user.user_name.toLowerCase() + "/tracks",
        beforeSend: function (request)
        {
            request.setRequestHeader("Token", TOKEN);
            request.setRequestHeader("Authorization", "Basic " + btoa(user.user_name + ":" + user.user_password));
        }

    }).done(function (data)
    {
        var track_list = $("#my_tracks");
        if (data.length === 0)
        {
            track_list.html("<li class='list-group-item'> No tracks found. </li>");
        }
        else
        {
            var my_tracks = data;
            track_list.html("");
            for (var i = 0; i < my_tracks.length; i++)
            {
                var track = my_tracks[i];
                track_list.append(buildTrackString(i, track));
                $("#my_track_" + i).click(displayTrackMap(i, track));
            }
        }
    });

}

function buildTrackString(id, track)
{

    var badges = "<span id='my_track_delete_badge' class='badge'>" +
        "<span id='my_track_delete_badge' class='glyphicon glyphicon-remove' style='color: #ef8888; font-size: 10px;'>" +
        "</span>" +
        "</span>";

    for (var i = track.track_keywords.length - 1; i >= 0; i--)
    {
        if (track.track_keywords[i] === "")
        {
            badges += "<span class='badge'><small>" + "---" + "</small></span>";
        }
        else
        {
            badges += "<span class='badge'><small>" + track.track_keywords[i] + "</small></span>";
        }
    }

    var track_string = "<li id='my_track_" + id + "' class='list-group-item'>" +
        "<strong class='tyt-blue' style='font-size: 14px; line-height: 24px;'>" + track.track_name + "</strong>" +
        badges + "<br>" +
        "<i>" + track.track_description + "<i>" +
        "<div id='my_tracks_map_" + id + "' class='mar-top-15 collapse' style='height: 300px; margin-bottom: 8px;'>" + "</div>" +
        "</li>";

    return track_string;
}

function displayTrackMap(id, track)
{
    var track_init_flag = false;

    return function (event)
    {
        if (event.target.id === "my_track_delete_badge")
        {
            $("#my_track_" + id).hide();
            deleteTrack(track.track_name);
        }
        else
        {
            $("#my_tracks_map_" + id).toggle();

            if (!track_init_flag)
            {
                initMyTracksMap(id, track);
                track_init_flag = true;
            }
        }
    };
}

function initMyTracksMap(id, track)
{

    var map = new google.maps.Map(document.getElementById("my_tracks_map_" + id),
    {
        disableDefaultUI: true
    });

    new google.maps.BicyclingLayer().setMap(map);

    var directions_renderer = new google.maps.DirectionsRenderer(
    {
        draggable: true
    });
    directions_renderer.setMap(map);

    directions_service = new google.maps.DirectionsService();

    MapUtils.importDirections(directions_renderer, directions_service, track.track_geojson, function () {});
}

function deleteTrack(track_name)
{
    $.ajax(
    {
        method: "DELETE",
        url: "https://trackyourtracks.eu-gb.mybluemix.net/api/tracks/" + window.encodeURIComponent(track_name).replace(/%20/g, '+'),
        beforeSend: function (request)
        {
            request.setRequestHeader("Token", TOKEN);
            request.setRequestHeader("Authorization", "Basic " + btoa(user.user_name + ":" + user.user_password));
        }

    }).done(function (data)
    {
        getMyTracks();
    });
}

function initNewTrackMap()
{

    new_track_map = new google.maps.Map(document.getElementById("new_track_map"),
    {
        disableDefaultUI: true
    });

    new google.maps.BicyclingLayer().setMap(new_track_map);

    new_track_directions_renderer = new google.maps.DirectionsRenderer(
    {
        draggable: true
    });
    new_track_directions_renderer.setMap(new_track_map);

    new_track_directions_service = new google.maps.DirectionsService();
    new_track_directions_service.route(
    {
        origin: 'HAW Hamburg',
        destination: 'Uni Hamburg',
        travelMode: 'BICYCLING'
    }, function (directions, status)
    {
        if (status == 'OK')
        {
            new_track_directions_renderer.setDirections(directions);
        }

    });

}

function createNewTrack(track)
{

    var button = $("#new_track_button");
    $.ajax(
    {
        method: "POST",
        url: "https://trackyourtracks.eu-gb.mybluemix.net/api/tracks",
        data: JSON.stringify(track),
        beforeSend: function (request)
        {
            request.setRequestHeader("Token", TOKEN);
            request.setRequestHeader("Authorization", "Basic " + btoa(user.user_name + ":" + user.user_password));
        }

    }).done(function ()
    {
        if (button.hasClass("btn-danger"))
        {
            button.removeClass("btn-danger");
            button.addClass("btn-success");
        }
        getMyTracks();

    }).fail(function ()
    {
        if (button.hasClass("btn-success"))
        {
            button.removeClass("btn-success");
            button.addClass("btn-danger");
        }
    });
}
