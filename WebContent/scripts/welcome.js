// WELCOME.JS

var TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0cmFja3lvdXJ0cmFja3MifQ.M6iIh58pjXUX4HrJvkfbI3iUyuNx8VKLy8MQCyQQVDA";

$(function ()
{

    // LOGIN

    ButtonToggle.init($("#login_button"), [0, 1]);

    $("#login_button").click(function ()
    {
        loginUser(
        {
            'user_name': $("#login_username").val(),
            'user_password': $("#login_password").val()
        });
    });

    // REGISTER

    ButtonToggle.init($("#register_button"), [2, 3, 4]);

    $("#register_button").click(function ()
    {
        registerUser(
        {
            'user_name': $("#register_username").val(),
            'user_mail': $("#register_email").val(),
            'user_password': $("#register_password").val()
        });
    });

});

function loginUser(user)
{
    var login_alert = $("#login_alert");
    $.ajax(
        {
            method: "GET",
            url: "https://trackyourtracks.eu-gb.mybluemix.net/api/users/" + user.user_name,
            beforeSend: function (request)
            {
                request.setRequestHeader("Token", TOKEN);
                request.setRequestHeader("Authorization", "Basic " + btoa(user.user_name + ":" + user.user_password));
            }

        }).done(function (data)
        {

            user.user_mail = data.user_mail;

            if (login_alert.is(':visible'))
            {
                login_alert.hide();
            }

            if ($("#login_remember_me").is(":checked"))
            {
                Storage.setUser("local_storage", user);
            }
            else
            {
                Storage.setUser("session_storage", user);
            }

            window.location = "dashboard.html";

        })
        .fail(function ()
        {
            if (!login_alert.is(':visible'))
            {
                login_alert.show();
            }
        });
}

function registerUser(user)
{
    var register_alert = $("#register_alert");
    $.ajax(
        {
            method: "POST",
            url: "https://trackyourtracks.eu-gb.mybluemix.net/api/users",
            data: JSON.stringify(user),
            beforeSend: function (request)
            {
                request.setRequestHeader("Token", TOKEN);
                request.setRequestHeader("Content-Type", "application/json");
            }

        }).done(function ()
        {
            if (register_alert.is(':visible'))
            {
                register_alert.hide();
            }

            if ($("#register_remember_me").is(":checked"))
            {
                Storage.setUser("local_storage", user);
            }
            else
            {
                Storage.setUser("session_storage", user);
            }

            window.location = "dashboard.html";
        })
        .fail(function ()
        {
            if (!register_alert.is(':visible'))
            {
                register_alert.show();
            }
        });
}
