var Storage = {

    GLOBAL_KEY: "TrackYourTracks",

    /* func   - Storage.isEmpty()
     * return - boolean
     */

    isEmpty: function ()
    {
        return Storage.get() === "no_storage";
    },

    /* func   - Storage.get()
     * return - storage
     */

    get: function ()
    {
        if (window.sessionStorage.getItem(Storage.GLOBAL_KEY) !== null)
        {
            return "session_storage";
        }
        else if (window.localStorage.getItem(Storage.GLOBAL_KEY) !== null)
        {
            return "local_storage";
        }
        else
        {
            return "no_storage";
        }
    },

    /* func   - Storage.clear(storage)
     * return -
     */

    clear: function ()
    {

        var storage = Storage.get();

        if (storage === "session_storage")
        {
            window.sessionStorage.clear();
        }
        else if (storage === "local_storage")
        {
            window.localStorage.clear();
        }
        else
        {
            throw "Invalid Storage Error";
        }
    },

    /* func   - Storage.getUser(storage)
     * return - user
     */

    getUser: function ()
    {
        var storage = Storage.get();

        if (storage === "session_storage")
        {
            return JSON.parse(window.sessionStorage.getItem(Storage.GLOBAL_KEY));
        }
        else if (storage === "local_storage")
        {
            return JSON.parse(window.localStorage.getItem(Storage.GLOBAL_KEY));
        }
        else
        {
            throw "Invalid Storage Error";
        }
    },

    /* func   - Storage.setUser(storage, user)
     * return -
     */

    setUser: function (storage, user)
    {
        if (storage === "session_storage")
        {
            window.sessionStorage.setItem(Storage.GLOBAL_KEY, JSON.stringify(user));
        }
        else if (storage === "local_storage")
        {
            window.localStorage.setItem(Storage.GLOBAL_KEY, JSON.stringify(user));
        }
        else
        {
            throw "Invalid Storage Error";
        }
    }

};
