/*
 * class - MapUtils.js
 * desc  - Utility class for using the Google Maps API
 * by    - Mark Tiedemann
 */


var MapUtils = {

  /*
   * func   - MapUtils.exportDirections(directions_renderer)
   * return - {
   *            start: [x, y],
   *            end: [x, y],
   *            waypoints: [[x, y], ...]
   *          }
   */

  exportDirections: function(directions_renderer) {

    var leg = directions_renderer.directions.routes[0].legs[0];
    var directions = {};
    directions.start = [leg.start_location.lat(), leg.start_location.lng()];
    directions.end = [leg.end_location.lat(), leg.end_location.lng()];
    directions.waypoints = [];
    for (var i = 0; i < leg.via_waypoints.length; i++)
      directions.waypoints[i] = [leg.via_waypoints[i].lat(), leg.via_waypoints[i].lng()];

    return directions;
  },

  /*
   * func   - MapUtils.importDirections(directions_renderer, directions_service, directions, callback)
   * return -
   */

  importDirections: function(directions_renderer, directions_service, directions, callback) {

    var waypoints = [];
    for (var i = 0; i < directions.waypoints.length; i++) {
      waypoints[i] = {
        location: new google.maps.LatLng(directions.waypoints[i][0], directions.waypoints[i][1]),
        stopover: false
      };
    }

    directions_service.route({
      origin: new google.maps.LatLng(directions.start[0], directions.start[1]),
      destination: new google.maps.LatLng(directions.end[0], directions.end[1]),
      waypoints: waypoints,
      travelMode: 'BICYCLING'
    }, function(directions, status) {
      if (status == 'OK') {
        directions_renderer.setDirections(directions);
        callback();
      }
    });

  },

  /*
   * func   - MapsElevation.calculateElevations(directions_renderer, elevation_count, elevations_callback)
   * return -
   */

  calculateElevations: function(directions_renderer, elevation_count, elevations_callback) {

    var leg = directions_renderer.directions.routes[0].legs[0];
    var elevation_service = new google.maps.ElevationService();
    elevation_service.getElevationAlongPath({
      path: [leg.start_location].concat(leg.via_waypoints, [leg.end_location]),
      samples: elevation_count
    }, function(elevation_results, status) {
      if (status == 'OK') {
        var elevations = [];
        for (var i = 0; i < elevation_results.length; i++) {
          elevations[i] = elevation_results[i].elevation;
        }
        elevations_callback(elevations);
      }
    });

  }

};
