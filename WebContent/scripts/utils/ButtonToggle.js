/*
 * class - ButtonToggle.js
 * desc  - Utility class for toggling a button based on inputs
 * by    - Mark Tiedemann
 */

var ButtonToggle = {

    /*
     * func   - ButtonToggle.init(button, input_numbers)
     *            button: the button to be toggled
     *            input_numbers: an array containing the numbers of the input fields to be used for toggling the button
     * return -
     */

    init: function (button, input_numbers)
    {
        var inputs = [];
        for (var i = 0; i < input_numbers.length; i++)
        {
            var input_field = $("input")[input_numbers[i]];
            ButtonToggle.initInput(button,
            {
                'field': input_field,
                'is_empty': input_field.value === ""
            }, inputs);
        }

        if (ButtonToggle.allFull(inputs))
            if (button.hasClass("disabled"))
                button.removeClass("disabled");
    },

    initInput: function (button, input, inputs)
    {
        input.field.oninput = function ()
        {
            if (input.field.value !== "")
            {
                input.is_empty = false;
                if (ButtonToggle.allFull(inputs))
                    if (button.hasClass("disabled"))
                        button.removeClass("disabled");
            }
            else
            {
                input.is_empty = true;
                if (!button.hasClass("disabled"))
                    button.addClass("disabled");
            }

        };

        inputs.push(input);
    },

    allFull: function (inputs)
    {
        for (var i = 0; i < inputs.length; i++)
        {
            if (inputs[i].is_empty)
            {
                return false;
            }
        }
        return true;
    }

};
