/*
 * class - WeatherUtils.js
 * desc  - Utility class for using the Yahoo Weather API
 * by    - Mark Tiedemann
 */


var WeatherUtils = {

  /*
   * func   - WeatherUtils.forecast(x, y, callback)
   * return - Returns weather data
   */

  forecast: function(x, y, callback) {

    var api_base = "https://query.yahooapis.com/v1/public/yql?q=";
    var geo_query = "SELECT * FROM weather.forecast WHERE woeid IN (SELECT woeid FROM geo.placefinder WHERE text=\"" + x + "," + y + "\" AND gflags=\"R\") AND u=\"c\"";
    var json_option = "&format=json";

    $.get(api_base + encodeURIComponent(geo_query) + json_option, function(response) {
      var forecast = response.query.results.channel.item.forecast[1];
      callback({
        low: forecast.low,
        high: forecast.high,
        condition: forecast.text
      });
    });
  }

};
