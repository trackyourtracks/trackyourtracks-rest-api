package de.haw.trackyourtracks.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureException;




public class Tokens {

	// Signatur Key
	// TODO Muss noch dringend au�erhalb des Quellcodes gespeichert werden!
	// Wird blo� zu testzwecken direkt hier erzeugt!
	static byte[] sigkey = "hfsuihfoisfiubsfnoasnfoisfngon".getBytes();

	public static boolean validateToken(String token) {

		try {
			Jwts.parser().setSigningKey(sigkey).parse(token);
			return true;
		} catch (SignatureException e) {
			return false;
		}
	}

	public static boolean ckeckPermission(String token, String communityName) {

		return Jwts.parser().setSigningKey(sigkey).parseClaimsJws(token).getBody().getSubject().equals(communityName);
	}

	public static String getCommunityNameFromToken(String token) {
		return Jwts.parser().setSigningKey(sigkey).parseClaimsJws(token).getBody().getSubject();
	}

	public static byte[] getSigkey() {
		return sigkey;
	}
	
	

}
