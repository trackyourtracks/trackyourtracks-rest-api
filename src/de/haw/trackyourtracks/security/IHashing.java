package de.haw.trackyourtracks.security;

public interface IHashing {

	public boolean checkHash(String plainText, String hashedText);

	public String hash(String plainText);

}
