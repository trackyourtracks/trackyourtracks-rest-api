package de.haw.trackyourtracks.database;

import java.util.List;

import de.haw.trackyourtracks.resources.checks.Creds;
import de.haw.trackyourtracks.resources.models.Community;
import de.haw.trackyourtracks.resources.models.Query;
import de.haw.trackyourtracks.resources.models.Track;
import de.haw.trackyourtracks.resources.models.User;

public interface IDatabase {

	public void addApiRequest(String communityName );
	
	public boolean containsUserName(String user_name, String communityName);

	public boolean containsUserMail(String user_mail, String communityName);

	public boolean containsTrackName(String track_name,String communityName);

	public boolean authUser(Creds creds,String communityName);

	public void createUser(User user, String communityName);

	public void createTrack(String user_name, Track track,String communityName);

	public void updateUser(String user_name, User user,String communityName);

	public void updateTrack(String user_name, Track track,String communityName);

	public User getUser(String user_name, String communityName);

	public Track getTrack(String track_name,String communityName);

	public void deleteUser(String user_name,String communityName);
	
	public void deleteCommunity(String communityName, String adminName);

	public void deleteTrack(String track_name,String communityName);

	public List<Track> getTracks(String user_name,String communityName);

	public void addTrack(String user_name, String track_name,String communityName);

	public void removeTrack(String user_name, String track_name,String communityName);

	public List<Track> queryTracks(Query query,String communityName);
	
	public void createNewDatabase(String dbname);
	
	public void createCommunity(Community community);
	
	public void syncDesignDocuments(String dbname) ;

	public Community getCommunity(String communityName);
	
	public boolean existCommunity(String communityName);

	public void updateCommunity(Community newCommunity, String community_name);

	void incrementUserCounter(String communityName);

	int getCommunityUserCounter(String communityName);

	void DecrementUserCounter(String communityName);

}