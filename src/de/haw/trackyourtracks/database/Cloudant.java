package de.haw.trackyourtracks.database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.joda.time.LocalDate;

import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import com.cloudant.client.api.Search;
import com.cloudant.client.api.model.DesignDocument;
import com.google.gson.JsonObject;

import de.haw.trackyourtracks.json.JSON;
import de.haw.trackyourtracks.resources.checks.Creds;
import de.haw.trackyourtracks.resources.models.Community;
import de.haw.trackyourtracks.resources.models.Query;
import de.haw.trackyourtracks.resources.models.Track;
import de.haw.trackyourtracks.resources.models.User;
import de.haw.trackyourtracks.security.BCryptHashing;
import de.haw.trackyourtracks.security.IHashing;
import de.haw.trackyourtracks.security.Tokens;
import de.haw.trackyourtracks.slack.SlackHook;
import de.haw.trackyourtracks.slack.SlackIcons;
import de.haw.trackyourtracks.slack.SlackPost;

public enum Cloudant implements IDatabase {

	DATABASE;

	private IHashing hashing = new BCryptHashing();
	private CloudantClient client = initClient();

	private CloudantClient initClient() {

		JsonObject environment = JSON.stringToJsonObject(System.getenv("VCAP_SERVICES"));
		JsonObject database = environment.getAsJsonArray("cloudantNoSQLDB").get(0).getAsJsonObject();
		JsonObject credentials = database.getAsJsonObject("credentials");
		String username = credentials.get("username").getAsString();
		String password = credentials.get("password").getAsString();

		doSlackPost();

		return new CloudantClient(username, username, password);
	}

	private void doSlackPost() {

		String url = "https://hooks.slack.com/services/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";
		SlackHook hook = new SlackHook(url);

		SlackPost post = new SlackPost();
		post.setTarget("#bluemix");
		post.setName("BLUEMIX BOT");
		post.setIcon(SlackIcons.BLUEMIX);
		post.setMessage("New Push [" + getDate() + "]");

		try {
			hook.post(post);
		} catch (IOException e) {
		}
	}

	private String getDate() {

		DateFormat format = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
		Date date = new Date();
		return format.format(date);
	}

	private String getIDFromTrackname(String track_name, String communityName) {
		return getTrack(track_name, communityName).getId();
	}

	private String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}
	
	
	@Override
	public void addApiRequest(String communityName) {
		
		Community com = getCommunity(communityName);
		
		if(com.getApiRequests()==null){
			com.initApiRequests();
		}
		
		LocalDate date = new LocalDate();
		
		HashMap<String,Integer> api_requests = com.getApiRequests();
		
		//Check is date already exist?
		if(api_requests.get(date.toString()) != null){
			api_requests.put(date.toString(), api_requests.get(date.toString())+1); //increment api request
		}else{
		   api_requests.put(date.toString(), 1);
		}
		
		
		//update Commuity in DB
		Database db = client.database("communitys", false);
		db.update(com);
		
	}
	
	
	@Override
	public void incrementUserCounter(String communityName) {
		
		Community com = getCommunity(communityName);
		com.incrementUser();
	
		//update Commuity in DB
		Database db = client.database("communitys", false);
		db.update(com);
		
	}
	
	@Override
	public void DecrementUserCounter(String communityName) {
		
		Community com = getCommunity(communityName);
		com.decrementUser();
	
		//update Commuity in DB
		Database db = client.database("communitys", false);
		db.update(com);
		
	}
	
	@Override
	public int getCommunityUserCounter(String communityName) {
		
		Community com = getCommunity(communityName);
		return com.getUser();
		
	}
	

	@Override
	public void addTrack(String user_name, String track_name, String communityName) {

		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
	
				
		
		Database db = client.database(communityName, false);
		User user = getUser(user_name, communityName);
		user.addTrackID(getIDFromTrackname(track_name, communityName));
		db.update(user);
	}

	@Override
	public boolean authUser(Creds creds, String communityName) {

		if (getUser(creds.getName(), communityName) == null)
			return false;

		String hashedText = getUser(creds.getName(), communityName).getPassword();

		return hashing.checkHash(creds.getPassword(), hashedText);
	}

	@Override
	public boolean containsTrackName(String track_name, String communityName) {

		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
		
		Database db = client.database(communityName, false);
		List<Track> tracks = db.view("contains/track_name").includeDocs(true).key(track_name).query(Track.class);
		return tracks.size() == 1;
	}

	@Override
	public boolean containsUserMail(String user_mail, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
		
		Database db = client.database(communityName, false);
		List<User> users = db.view("contains/user_mail").includeDocs(true).key(user_mail).query(User.class);
		return users.size() == 1;
	}

	public boolean containsUserName(String user_name, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
					
		Database db = client.database(communityName, false);
		List<User> users = db.view("contains/user_name").includeDocs(true).key(user_name).query(User.class);
		return users.size() == 1;
	}

	@Override
	public void createCommunity(Community community) {
		Database db = client.database("communitys", false);
		community.setUserPassword(hashing.hash(community.getUserPassword()));
		db.post(community);
	}

	public void createNewDatabase(String dbname) {
		client.createDB(dbname);
	}

	@Override
	public void createTrack(String user_name, Track track, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
		
		Database db = client.database(communityName, false);
		User user = getUser(user_name, communityName);

		track.setCreator(user.getId());
		track.setId(client.uuids(1).get(0));

		user.addTrackID(track.getId());

		db.update(user);
		db.post(track);
	}

	@Override
	public void createUser(User user, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
		
		Database db = client.database(communityName, false);
		user.setPassword(hashing.hash(user.getPassword()));
		user.initTrackIDs();

		db.post(user);
	}

	@Override
	public void deleteCommunity(String communityName, String adminName) {

		//isPublic community?
		if(!getCommunity(communityName).isPub()){
				client.deleteDB(communityName, "delete database");
	    }
		
		// Delete admin
		Database db = client.database("communitys", false);
		User admin = getUser(adminName, "communitys");
		db.remove(admin.getId(), admin.getRevision());
	}

	@Override
	public void deleteTrack(String track_name, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
		
		Database db = client.database(communityName, false);
		// Delete track references in user docs
		List<User> users = db.view("get/userToTrackID").key(getIDFromTrackname(track_name, communityName)).includeDocs(true).query(User.class);
		for (User user : users) {
			user.deleteID(getIDFromTrackname(track_name, communityName));
			db.update(user);
		}

		// remove track document from database
		db.remove(getTrack(track_name, communityName));
	}

	@Override
	public void deleteUser(String user_name, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
		
		Database db = client.database(communityName, false);
		db.remove(getUser(user_name, communityName));
	}

	@Override
	public boolean existCommunity(String communityName) {
		Database db = client.database("communitys", false);
		List<Community> community = db.view("get/community").key(communityName).includeDocs(true).query(Community.class);
		return community.size() > 0;
	}

	@Override
	public Community getCommunity(String communityName) {
		Database db = client.database("communitys", false);
		List<Community> community = db.view("get/community").key(communityName).includeDocs(true).query(Community.class);
		return community.iterator().next();
	}

	@Override
	public Track getTrack(String track_name, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
		
		Database db = client.database(communityName, false);
		List<Track> tracks = db.view("get/track").key(track_name).includeDocs(true).query(Track.class);
		
		if(tracks.size()==0)
			return null;
		
		return tracks.iterator().next();
	}

	@Override
	public List<Track> getTracks(String user_name, String communityName) {
		
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
		
		Database db = client.database(communityName, false);
		HashSet<String> ids = getUser(user_name, communityName).getTrackIds();

		ArrayList<Track> tracks = new ArrayList<Track>();

		for (String id : ids) {
			tracks.add(JSON.toObject(getStringFromInputStream(db.find(id)), Track.class));
		}

		return tracks;
	}

	@Override
	public User getUser(String user_name, String communityName) {
		
		//isPublic community?
		if(!communityName.equals("communitys") && getCommunity(communityName).isPub()){
			communityName = "pub";
		}
		
		Database db = client.database(communityName, false);
		List<User> users = db.view("get/user").key(user_name).includeDocs(true).query(User.class);
		if (users.size() == 0)
			return null;

		return users.iterator().next();
	}
		
	

	@Override
	public List<Track> queryTracks(Query query, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
		
		Database db = client.database(communityName, false);
		StringBuilder lucene = new StringBuilder();

		// If query emty return just all Tracks from the community
		if (query.isNull()) {
			List<Track> tracks = db.view("get/track").includeDocs(true).query(Track.class);
			return tracks;
		}

		// matches_name
		if (query.getMatchesName() != null) {
			if (lucene.length() > 0)
				lucene.append("AND ");

			lucene.append("track_name:" + query.getMatchesName() + "*");
		}

		// length_greater_than
		if (query.getLengthGreaterThan() != null) {
			if (lucene.length() > 0)
				lucene.append("AND ");

			lucene.append("track_length:{" + query.getLengthGreaterThan() + " TO Infinity}");
		}

		// length_less_than
		if (query.getLengthLessThan() != null) {
			if (lucene.length() > 0)
				lucene.append("AND ");

			lucene.append("track_length:{-Infinity TO " + query.getLengthLessThan() + "}");

		}

		if (lucene.length() == 0) {
			return null;
		}

		Search search = db.search("search/tracks");
		List<Track> rslt = search.includeDocs(true).query(lucene.toString(), Track.class);

		return rslt;

	}

	@Override
	public void removeTrack(String user_name, String track_name, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
				communityName = "pub";
		}

		Database db = client.database(communityName, false);
		User user = getUser(user_name, communityName);
		user.deleteID(getIDFromTrackname(track_name, communityName));
		db.update(user);
	}

	@Override
	public void syncDesignDocuments(String dbname) {
		/*
		 * Dont use! DesignDocument designDoc = new DesignDocument();
		 * 
		 * InputStream test =
		 * this.getClass().getResourceAsStream("/search.json");
		 * 
		 * BufferedReader reader = new BufferedReader(new
		 * InputStreamReader(test)); StringBuilder out = new StringBuilder();
		 * String line; try { while ((line = reader.readLine()) != null) {
		 * out.append(line); }
		 * 
		 * reader.close(); } catch (IOException e) { e.printStackTrace(); }
		 * 
		 * String s = out.toString();
		 * 
		 * JsonObject o = new JsonParser().parse(s).getAsJsonObject();
		 * designDoc.setFulltext(o); String asa = o.toString();
		 */

		// TODO Rausfinden wo die Datei denn nun hinterlegt sein muss und dann
		// den token sign key so auszulesen

		// TODO Einfach ersetzten durch Datenbankreplikation!

		// _design/search
		Database template = client.database("template", false);
		DesignDocument designDoc = template.design().getFromDb("_design/search");
		designDoc.setRevision(null); // a new Document need a null Rev!

		// _design/contains
		DesignDocument designDoc2 = template.design().getFromDb("_design/contains");
		designDoc2.setRevision(null); // a new Document need a null Rev!

		// _design/get
		DesignDocument designDoc3 = template.design().getFromDb("_design/get");
		designDoc3.setRevision(null); // a new Document need a null Rev!

		// Sync with new Database
		Database newdb = client.database(dbname, false);
		newdb.design().synchronizeWithDb(designDoc);
		newdb.design().synchronizeWithDb(designDoc2);
		newdb.design().synchronizeWithDb(designDoc3);

	}

	@Override
	public void updateCommunity(Community newcommunity, String communityName) {
		Database db = client.database("communitys", false);

		Community updateCommunity = getCommunity(communityName);
		updateCommunity.setUserName(newcommunity.getUserName());
		updateCommunity.setUserPassword(hashing.hash(newcommunity.getUserPassword()));

		db.update(updateCommunity);
	}

	@Override
	public void updateTrack(String old_track_name, Track track, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
			communityName = "pub";
		}
				
		Database db = client.database(communityName, false);
		Track oldTrack = getTrack(old_track_name, communityName);
		// Change trackname?
		if (track.getName() != null) {
			oldTrack.setName(track.getName());
		}
		// Change trackdescription?
		if (track.getDescription() != null) {
			oldTrack.setDescription(track.getDescription());
		}
		// Change track_keywords?
		if (track.getKeywords() != null) {
			oldTrack.setKeywords(track.getKeywords());
		}
		// Change track_geojson?
		if (track.getGeojson() != null) {
			oldTrack.setGeojson(track.getGeojson());
		}

		db.update(oldTrack);
	}

	@Override
	public void updateUser(String old_user_name, User user, String communityName) {
		
		//isPublic community?
		if(getCommunity(communityName).isPub()){
				communityName = "pub";
		}
				
				
		Database db = client.database(communityName, false);

		User olduser = getUser(old_user_name, communityName);

		// Change user_password?
		if (user.getPassword() != null) {
			olduser.setPassword(hashing.hash(user.getPassword()));
		}

		// Change username?
		if (user.getName() != null) {
			olduser.setName(user.getName());
		}

		// Change User_mail?
		if (user.getMail() != null) {
			olduser.setMail(user.getMail());
		}

		db.update(olduser);
	}


}
