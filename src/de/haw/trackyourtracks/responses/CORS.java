package de.haw.trackyourtracks.responses;

import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.ResponseBuilder;

public abstract class CORS {

	public static ResponseBuilder add(ResponseBuilder builder, HttpHeaders headers) {

		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS#Access-Control-Allow-Headers

		List<String> requestHeaders = headers.getRequestHeader("Access-Control-Request-Headers");

		if (requestHeaders != null && !requestHeaders.isEmpty()) {
			String allowHeaders = requestHeaders.toString();
			// remove brackets
			allowHeaders = allowHeaders.substring(1, allowHeaders.length() - 1);
			builder.header("Access-Control-Allow-Headers", allowHeaders);
		}

		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS#Requests_with_credentials

		List<String> originHeaders = headers.getRequestHeader("Origin");

		if (originHeaders != null && !originHeaders.isEmpty()) {
			String origin = originHeaders.get(0);
			builder.header("Access-Control-Allow-Origin", origin);
		}

		builder.header("Access-Control-Allow-Credentials", "true");
		builder.header("Access-Control-Allow-Methods", "POST, PUT, GET, DELETE, OPTIONS");

		return builder;
	}

}
