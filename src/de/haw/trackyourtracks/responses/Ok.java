package de.haw.trackyourtracks.responses;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import de.haw.trackyourtracks.json.JSON;

public class Ok {

	public static Response HTTP_200(HttpHeaders headers) {
		return Ok.create(200, headers).build();
	}

	public static Response HTTP_200(Object entity, HttpHeaders headers) {

		ResponseBuilder builder = Ok.create(200, headers);
		builder.header("Content-Type", "application/json");
		return builder.entity(JSON.objectToString(entity)).build();
	}

	public static Response HTTP_201(HttpHeaders headers) {
		return Ok.create(201, headers).build();
	}
	
	public static Response HTTP_201(Object entity,HttpHeaders headers) {
		ResponseBuilder builder = Ok.create(201, headers);
		builder.header("Content-Type", "application/json");
		return builder.entity(JSON.objectToString(entity)).build();
	}

	public static Response HTTP_204(HttpHeaders headers) {
		return Ok.create(204, headers).build();
	}

	private static ResponseBuilder create(int status, HttpHeaders headers) {
		return CORS.add(Response.status(status), headers);
	}

}
