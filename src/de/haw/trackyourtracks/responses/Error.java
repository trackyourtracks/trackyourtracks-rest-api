package de.haw.trackyourtracks.responses;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.google.gson.JsonObject;

import de.haw.trackyourtracks.json.JSON;

public class Error {

	public static Response HTTP_400(HttpHeaders headers) {
		return Error
				.build(400, "Bad Request", "Request was malformed", headers);
	}

	public static Response HTTP_401(HttpHeaders headers) {
		return Error.build(401, "Unauthorized",
				"No or wrong authentication provided", headers);
	}

	public static Response HTTP_403(HttpHeaders headers) {
		return Error.build(403, "Forbidden", "Access to resource is forbidden",
				headers);
	}

	public static Response HTTP_404(HttpHeaders headers) {
		return Error.build(404, "Not Found", "Resource was not found", headers);
	}

	public static Response HTTP_409(boolean track_name_taken,String conflictItem,
			HttpHeaders headers) {

		JsonObject object = new JsonObject();
		object.addProperty(conflictItem, track_name_taken);

		Error.addJson(object, "Conflict", "Request resulted in a conflict");
		String error = JSON.objectToString(object);

		ResponseBuilder builder = CORS.add(Response.status(409), headers);
		builder.header("Content-Type", "application/json");
		return builder.entity(error).build();
	}

	public static Response HTTP_409(boolean user_name_taken,
			boolean user_mail_taken, HttpHeaders headers) {

		JsonObject object = new JsonObject();
		object.addProperty("user_name_taken", user_name_taken);
		object.addProperty("user_mail_taken", user_mail_taken);

		Error.addJson(object, "Conflict", "Request resulted in a conflict");
		String error = JSON.objectToString(object);

		ResponseBuilder builder = CORS.add(Response.status(409), headers);
		builder.header("Content-Type", "application/json");
		return builder.entity(error).build();
	}

	public static Response HTTP_505(HttpHeaders headers) {
		return Error.build(505, "HTTP Version Not Supported",
				"Request must be via HTTPS", headers);
	}

	private static Response build(int status, String type, String message,
			HttpHeaders headers) {

		JsonObject object = new JsonObject();
		Error.addJson(object, type, message);
		String error = JSON.objectToString(object);

		ResponseBuilder builder = CORS.add(Response.status(status), headers);
		builder.header("Content-Type", "application/json");
		return builder.entity(error).build();
	}

	private static void addJson(JsonObject object, String type, String message) {

		JsonObject error = new JsonObject();
		error.addProperty("error_type", type);
		error.addProperty("error_message", message);
		object.add("error", error);
	}

}
