package de.haw.trackyourtracks.slack;

public abstract class SlackIcons {

	public static final String BLUEMIX = "http://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/IBM_Bluemix_logo.svg/356px-IBM_Bluemix_logo.svg.png";
	public static final String ECLIPSE = "http://git.eclipse.org/c/platform/eclipse.platform.git/plain/platform/org.eclipse.platform/eclipse256.png";
	public static final String TRACKYOURTRACKS = "https://trackyourtracks.eu-gb.mybluemix.net/images/trackyourtracks-logo.png";
	public static final String BITBUCKET = "https://gitlab.com/assets/authbuttons/bitbucket_64-cccc800d2b8759c64dfd37e0cc91babb.png";

}
