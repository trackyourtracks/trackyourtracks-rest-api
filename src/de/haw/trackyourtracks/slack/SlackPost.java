package de.haw.trackyourtracks.slack;

import com.google.gson.annotations.SerializedName;

public class SlackPost {

	@SerializedName("channel")
	private String target;

	@SerializedName("username")
	private String name;

	@SerializedName("icon_url")
	private String icon;

	@SerializedName("text")
	private String message;

	public String getTarget() {
		return target;
	}

	public String getName() {
		return name;
	}

	public String getIcon() {
		return icon;
	}

	public String getMessage() {
		return message;
	}

	public void setTarget(String target) {
		
		if (!target.startsWith("#") && !target.startsWith("@"))
			throw new IllegalArgumentException("target must start with '#' or '@'");

		this.target = target;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public void setMessage(String message) {
		
		if (message == null || message.isEmpty())
			throw new IllegalArgumentException("message may not be null or empty");
		
		this.message = message;
	}
	
}
