package de.haw.trackyourtracks.slack;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;

import com.google.gson.Gson;

public class SlackHook {

	private HttpClient client = HttpClients.createDefault();
	private Gson gson = new Gson();

	private String url;

	public SlackHook(String url) {
		this.url = url;
	}

	public int post(SlackPost slackPost) throws IOException {

		if (slackPost.getMessage() == null)
			throw new IllegalArgumentException("SlackPost must have a message");

		HttpPost post = new HttpPost(url);
		post.setHeader("Content-Type", "application/json");
		post.setEntity(new StringEntity(gson.toJson(slackPost)));

		HttpResponse response = client.execute(post);
		return response.getStatusLine().getStatusCode();
	}
}
