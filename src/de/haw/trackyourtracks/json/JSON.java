package de.haw.trackyourtracks.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public abstract class JSON {

	private static final Gson GSON = initGson();

	private static Gson initGson() {

		GsonBuilder builder = new GsonBuilder().setPrettyPrinting();
		builder.addSerializationExclusionStrategy(new JSONSerializationExclusionStrategy());
		return builder.create();
	}

	public static String objectToString(Object object) {
		try {
			return GSON.toJson(object);
		} catch (Exception e) {
			return null;
		}
	}

	public static JsonObject objectToJsonObject(Object object) {
		try {
			// TODO -- FIND DIRECT COVERSATION
			return JSON.stringToJsonObject(JSON.objectToString(object));
		} catch (Exception e) {
			return null;
		}
	}

	public static JsonObject stringToJsonObject(String json) {
		try {
			return new JsonParser().parse(json).getAsJsonObject();
		} catch (Exception e) {
			return null;
		}
	}

	public static <T> T toObject(String json, Class<T> clazz) {
		try {
			return GSON.fromJson(json, clazz);
		} catch (Exception e) {
			return null;
		}
	}

	public static JsonObject pair(String key, String value) {

		JsonObject pair = new JsonObject();
		pair.addProperty(key, value);
		return pair;
	}

}
