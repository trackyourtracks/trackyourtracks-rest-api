package de.haw.trackyourtracks.json;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

import de.haw.trackyourtracks.resources.models.Track;
import de.haw.trackyourtracks.resources.models.User;

public class JSONSerializationExclusionStrategy implements ExclusionStrategy {

	@Override
	public boolean shouldSkipField(FieldAttributes fieldAttributes) {

		if ("_id".equals(fieldAttributes.getName()))
			return true;

		if ("_rev".equals(fieldAttributes.getName()))
			return true;

		if (User.class.equals(fieldAttributes.getDeclaringClass())) {

			if ("user_password".equals(fieldAttributes.getName()))
				return true;

		}

		if (Track.class.equals(fieldAttributes.getDeclaringClass())) {

			if ("track_creator".equals(fieldAttributes.getName()))
				return true;

		}

		return false;
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}

}
