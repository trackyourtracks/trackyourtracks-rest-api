package de.haw.trackyourtracks.resources;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import de.haw.trackyourtracks.database.Cloudant;
import de.haw.trackyourtracks.database.IDatabase;
import de.haw.trackyourtracks.json.JSON;
import de.haw.trackyourtracks.resources.checks.Creds;
import de.haw.trackyourtracks.resources.checks.Protocol;
import de.haw.trackyourtracks.resources.models.Community;
import de.haw.trackyourtracks.resources.models.Token;
import de.haw.trackyourtracks.resources.models.User;
import de.haw.trackyourtracks.responses.Error;
import de.haw.trackyourtracks.responses.Ok;
import de.haw.trackyourtracks.security.Tokens;

@Path("/communities")
public class Communities {

	private IDatabase database = Cloudant.DATABASE;

	@POST
	public Response addCommunity(@Context HttpHeaders headers, String body) {

		if (!Protocol.isHttps(headers))
			return Error.HTTP_505(headers);

		Community community = JSON.toObject(body, Community.class);

		if (community == null || !community.isPopulated())
			return Error.HTTP_400(headers);

		// set to lower case
		community.setCommunityName(community.getCommunityName().toLowerCase());
		community.setUserName(community.getUserName().toLowerCase());

		// Exist already?
		if (database.existCommunity(community.getCommunityName()))
			return Error.HTTP_409(true, "community_name_taken", headers);
		// TODO Error 409 f�r communitys implementieren

		// Generate token
		String token = Jwts.builder().setSubject(community.getCommunityName()).signWith(SignatureAlgorithm.HS256, Tokens.getSigkey()).compact();

		
		//If Private community?
		if(!community.isPub()){
			database.createNewDatabase(community.getCommunityName());
			database.syncDesignDocuments(community.getCommunityName());
		}
		
		// Add a community object to the "communitys" database
		database.createCommunity(community);

		return Ok.HTTP_201(new Token(token), headers);

	}

	@GET
	@Path("{community_name}")
	public Response getToken(@PathParam("community_name") String community_name, @Context HttpHeaders headers) {

		if (!Protocol.isHttps(headers))
			return Error.HTTP_505(headers);

		Response auth = auth(headers, community_name);

		if (auth.getStatus() != 200)
			return auth;

		String token = Jwts.builder().setSubject(community_name).signWith(SignatureAlgorithm.HS256, Tokens.getSigkey()).compact();

		return Ok.HTTP_200(new Token(token), headers);
	}

	@DELETE
	@Path("{community_name}")
	public Response deleteCommunity(@PathParam("community_name") String community_name, @Context HttpHeaders headers) {

		if (!Protocol.isHttps(headers))
			return Error.HTTP_505(headers);

		community_name = community_name.toLowerCase();

		Response auth = auth(headers, community_name);

		if (auth.getStatus() != 200)
			return auth;

		database.deleteCommunity(community_name, Creds.parse(headers).getName());

		return Ok.HTTP_204(headers);
	}

	
	@PUT
	@Path("{community_name}")
	public Response updateCommunity(@PathParam("community_name") String community_name, @Context HttpHeaders headers, String body) {

		Response auth = auth(headers, community_name);

		if (auth.getStatus() != 200)
			return auth;

		Community newCommunity = JSON.toObject(body, Community.class);

	    //Body malformed?
		if(newCommunity.getUserName()==null || newCommunity.getUserPassword() == null)
			return Error.HTTP_400(headers);

		
		database.updateCommunity(newCommunity,community_name);

		return Ok.HTTP_200(headers);
	}
	
	
	
	private Response auth(HttpHeaders headers, String community_name) {

		if (!Protocol.isHttps(headers))
			return Error.HTTP_505(headers);

		Creds creds = Creds.parse(headers);

		if (creds == null)
			return Error.HTTP_401(headers);

		if (!database.getCommunity(community_name).getUserName().equals(creds.getName()))
			// TODO abfangen, wenn die community nicht existiert
			return Error.HTTP_403(headers);

		if (!database.authUser(creds, "communitys"))
			return Error.HTTP_401(headers);

		return Ok.HTTP_200(headers);
	}
	
	

	@OPTIONS
	public Response options(@Context HttpHeaders headers) {
		return Ok.HTTP_200(headers);
	}

	@OPTIONS
	@Path("{community_name}")
	public Response communityOptions(@Context HttpHeaders headers) {
		return Ok.HTTP_200(headers);
	}

}
