package de.haw.trackyourtracks.resources.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestCommunities.class, TestUsers.class, TestTracks.class })
public class AllTests {

}
