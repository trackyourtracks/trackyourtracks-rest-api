package de.haw.trackyourtracks.resources.test;

 import static com.jayway.restassured.RestAssured.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;

import de.haw.trackyourtracks.json.JSON;
import de.haw.trackyourtracks.resources.models.Community;

//Test Framework: REST Assured
//Tutorial: http://www.hameister.org/SpringMvcRESTassured.html
//JSON escape Generator: http://bernhardhaeussner.de/odd/json-escape/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCommunities {
	 
	    RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
	    
	    private Community test_community = new Community("test_community_name", "test_user_name", "test_user_password", false);
	    	    
	   @BeforeClass
	    public static void setupConnection() {
	        RestAssured.baseURI = "https://trackyourtracks.eu-gb.mybluemix.net";
	        RestAssured.basePath = "/api";     
	    }
	     
	    @Before
	    public void setup() throws Exception {
	        requestSpecBuilder.setContentType(ContentType.JSON).addHeader("Accept", ContentType.JSON.getAcceptHeader());
	    }
	
	
	@Test
	public void Test1createCommunity() {
				
		 given()
	        .spec(requestSpecBuilder.build()).body(JSON.objectToString(test_community))
	        .log().all()
	    .when()
	        .post("/communities")
	    .then()
	        .statusCode(201);
	}
	
	@Test
	public void Test2getToken() {
		 given()
	        .auth().preemptive().basic(test_community.getUserName(), test_community.getUserPassword())
	        .log().all()
	    .when()
	        .get("/communities/"+ test_community.getCommunityName())
	    .then()
	        .statusCode(200);
	}
	
	
	
	@Test
	public void Test4UpdateCommunity() {
		 given()
		     .spec(requestSpecBuilder.build()).body("{\n\"user_name\" : \"albert\",\n\"user_password\" : \"einstein\"\n}\n")
	        .auth().preemptive().basic(test_community.getUserName(), test_community.getUserPassword())
	        .log().all()
	    .when()
	        .put("/communities/" + test_community.getCommunityName())
	    .then()
	        .statusCode(200);
		 
		 
		 //rename to original name
		 given()
	        .spec(requestSpecBuilder.build()).body(JSON.objectToString(test_community))
	        .auth().preemptive().basic("albert", "einstein")
	        .log().all()
	    .when()
	         .put("/communities/" + test_community.getCommunityName());
	
		 
	}
	
	
	@Test
	public void Test5deleteCommunity() {
		 given()
	        .auth().preemptive().basic(test_community.getUserName(), test_community.getUserPassword())
	        .log().all()
	    .when()
	        .delete("/communities/" + test_community.getCommunityName())
	    .then()
	        .statusCode(204);
	}
	
}
