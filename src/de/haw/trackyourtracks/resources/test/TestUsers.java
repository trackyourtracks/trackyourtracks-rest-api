package de.haw.trackyourtracks.resources.test;

 import static com.jayway.restassured.RestAssured.*;
import static com.jayway.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.* ;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;

//Test Framework: REST Assured
//Tutorial: http://www.hameister.org/SpringMvcRESTassured.html
//JSON escape Generator: http://bernhardhaeussner.de/odd/json-escape/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestUsers {
	 
	    RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();
	    
	    public static final String community_name = "testcommunity2";
	    public static final String user_name = "testuser";
	    public static final String user_mail = "testuser@web.de";
	    public static final String user_password = "123";
	    
	    
	    
	    
	    @BeforeClass
	    public static void setupConnection() {
	        RestAssured.baseURI = "https://trackyourtracks.eu-gb.mybluemix.net";
	        RestAssured.basePath = "/api";  
	        RestAssured.authentication = basic(user_name,"123");
	    }
	     
	    @Before
	    public void setup() throws Exception {
	    	Response response = getToken();
	    	String token = response.getBody().jsonPath().getString("token");
			
	        requestSpecBuilder.setContentType(ContentType.JSON).addHeader("Accept", ContentType.JSON.getAcceptHeader()).addHeader("token", token);
	    }
	    
		
		public Response getToken() {
			Response response =
			given()
		        .auth().preemptive().basic("testAdmin2", "123")
		        .log().all()
		    .when()
		        .get("/communities/"+community_name+"");
			
			return response;
		}
		
	
	@Test
	public void Test11CreateUser() {
		given()
		    .auth().preemptive().basic(user_name, "123")
	        .spec(requestSpecBuilder.build()).body("{\n\"user_name\" : \""+user_name+"\",\n\"user_mail\" : \""+user_mail+"\",\n\"user_password\" : \""+user_password+"\"\n}")
	        .log().all()
	    .when()
	        .post("/users")
	    .then()
	        .statusCode(201);
	}
	
	
	//Create user malformed
	@Test
	public void Test12CreateUser() {
		given()
		    .auth().preemptive().basic(user_name, "123")
	        .spec(requestSpecBuilder.build()).body("{\n\"user_mail\" : \""+user_mail+"\",\n\"user_password\" : \""+user_password+"\"\n}")
	        .log().all()
	    .when()
	        .post("/users")
	    .then()
	        .statusCode(400);
	}
	
	@Test
	public void Test2GetUser() {
		given()
		    .auth().preemptive().basic(user_name, "123")
	        .spec(requestSpecBuilder.build())
	        .log().all()
	    .when()
	        .get("/users/"+user_name+"")
	    .then()
	        .statusCode(200)
	        .contentType(ContentType.JSON)
	        .body("user_name", is(user_name))
	        .body("user_mail", is(user_mail));
	        
	}
	


	//Update name mail and password
	@Test
	public void Test31UpdateUser() {
		given()
		    .auth().preemptive().basic(user_name, "123")
	        .spec(requestSpecBuilder.build()).body("{\n\"user_name\" : \"updatetTestUser\",\n\"user_mail\" : \"mailtestuser@bwe.de\",\n\"user_password\" : \"12345\"\n}")
	        .log().all()
	    .when()
	        .put("/users/"+user_name+"")
	    .then()
	        .statusCode(200);
		
	}
	
	
	//Update name mail and password
		@Test
		public void Test32ReUpdateUser() {
			given()
			    .auth().preemptive().basic("updatetTestUser", "12345")
		        .spec(requestSpecBuilder.build()).body("{\n\"user_name\" : \""+user_name+"\",\n\"user_mail\" : \""+user_mail+"\",\n\"user_password\" : \""+user_password+"\"\n}")
		        .log().all()
		    .when()
		        .put("/users/updatetTestUser")
		    .then()
		        .statusCode(200);	
		}
	

	//HTTPS error
	@Test
	public void Test4Code505() {
		requestSpecBuilder.setBaseUri("http://trackyourtracks.eu-gb.mybluemix.net");
		
		given()
		    .auth().preemptive().basic(user_name, "123")
	        .spec(requestSpecBuilder.build())
	        .log().all()
	    .when()
	        .get("/users/"+user_name+"")
	    .then()
	        .statusCode(505);
		
		requestSpecBuilder.setBaseUri("https://trackyourtracks.eu-gb.mybluemix.net");
	       
	        
	}
	
	//Unauthorized : Wrong password
	@Test
	public void Test51Code401GETuser() {
	
		given()
		    .auth().preemptive().basic(user_name, "wrongPassword")
	        .spec(requestSpecBuilder.build())
	        .log().all()
	    .when()
	        .get("/users/"+user_name+"")
	    .then()
	        .statusCode(401);
	       
	        
	}
	
	//Unauthorized : Wrong password
	@Test
	public void Test52Code401Updateuser() {
		
		given()
			  .auth().preemptive().basic(user_name, "wrongPassword")
		      .spec(requestSpecBuilder.build())
		      .log().all()
	     .when()
		      .put("/users/"+user_name+"")
		  .then()
		      .statusCode(401);
		       
		        
		}
		
		//Unauthorized : Wrong password
		@Test
		public void Test53Code401Deleteuser() {
				
		   given()
		   		.auth().preemptive().basic(user_name, "wrongPassword")
				.spec(requestSpecBuilder.build())
				 .log().all()
		   .when()
				  .put("/users/"+user_name+"")
				  .then()
				  .statusCode(401);			          
				}
	
	
	//Conflic : User alreay in database
	@Test
	public void Test6Code409() {
	
		given()
	    .auth().preemptive().basic(user_name, "123")
        .spec(requestSpecBuilder.build()).body("{\n\"user_name\" : \""+user_name+"\",\n\"user_mail\" : \""+user_mail+"\",\n\"user_password\" : \""+user_password+"\"\n}")
        .log().all()
    .when()
        .post("/users")
    .then()
        .statusCode(409);
	            
	}
	
	
	//Unauthorized : Wrong token
	@Test
	public void Test7Code401() {
	
		requestSpecBuilder.setContentType(ContentType.JSON).addHeader("Accept", ContentType.JSON.getAcceptHeader()).addHeader("token", "wrong token");
		
		given()
		    .auth().preemptive().basic(user_name, "123")
	        .spec(requestSpecBuilder.build())
	        .log().all()
	    .when()
	        .get("/users/"+user_name+"")
	    .then()
	        .statusCode(401);	 	
	        
	}
	
	
	//Access to resource is forbidden
	@Test
	public void Test8Code403() {
	
		given()
		    .auth().preemptive().basic(user_name, "123")
	        .spec(requestSpecBuilder.build())
	        .log().all()
	    .when()
	        .get("/users/alreadyexistuser")
	    .then()
	        .statusCode(403);	 	
	        
	}
	
	
	
	
	@Test
	public void Test9DeleteUser() {
		given()
		    .auth().preemptive().basic(user_name, "123")
	        .spec(requestSpecBuilder.build())
	        .log().all()
	    .when()
	        .delete("/users/"+user_name+"")
	    .then()
	        .statusCode(204);      
	}
	
	
	


}
