package de.haw.trackyourtracks.resources.test;

import static com.jayway.restassured.RestAssured.*;
import static com.jayway.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;

import de.haw.trackyourtracks.json.JSON;

//Test Framework: REST Assured
//Tutorial: http://www.hameister.org/SpringMvcRESTassured.html
//JSON escape Generator: http://bernhardhaeussner.de/odd/json-escape/
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestTracks {

	RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

	public static final String community_name = "testcommunity2";
	public static final String user_name = "alreadyexistuser";
	public static final String user_password = "123";

	public static final String wrong_user_name = "alreadyexistuser2";
	public static final String wrong_user_mail = "alreadyexistuser2@web.de";
	public static final String wrong_user_password = "123";

	public static final String track_name_0 = "alreadyexisttrack";

	public static final String track_name = "testtrack";
	public static final String track_description = "testdescription";
	public static final String[] track_keywords = { "kw1", "kw2", "kw3" };
	public static final String track_geojson = "{\n  \"type\": \"FeatureCollection\",\n  \"features\": []\n}";

	@BeforeClass
	public static void setupConnection() {
		RestAssured.baseURI = "https://trackyourtracks.eu-gb.mybluemix.net";
		RestAssured.basePath = "/api";

	}

	public Response getToken() {
		Response response = given().auth().preemptive().basic("testAdmin2", "123").log().all().when().get("/communities/" + community_name + "");

		return response;
	}

	@Before
	public void setup() throws Exception {
		Response response = getToken();
		String token = response.getBody().jsonPath().getString("token");

		requestSpecBuilder.setContentType(ContentType.JSON).addHeader("Accept", ContentType.JSON.getAcceptHeader()).addHeader("token", token);
	}

	@Test
	public void Test11CreateTrack() {
		given().auth().preemptive().basic(user_name, user_password).spec(requestSpecBuilder.build())
				.body("{\n  \"track_name\": " + track_name + ",\n  \"track_description\": " + track_description + ",\n  \"track_keywords\": " + JSON.objectToString(track_keywords) + ",\n  \"track_geojson\": " + track_geojson + "}").when().post("/tracks").then().statusCode(201);
	}

	@Test
	public void Test12CreateTrackNameAlreadyTaken() {
		given().auth().preemptive().basic(user_name, user_password).spec(requestSpecBuilder.build())
				.body("{\n  \"track_name\": " + track_name + ",\n  \"track_description\": " + track_description + ",\n  \"track_keywords\": " + JSON.objectToString(track_keywords) + ",\n  \"track_geojson\": " + track_geojson + "}").when().post("/tracks").then().statusCode(409);
	}

	@Test
	public void Test13CreateTrackNullObject() {
		given().auth().preemptive().basic(user_name, user_password).spec(requestSpecBuilder.build()).body("{}").log().all().when().post("/tracks").then().statusCode(400);
	}

	@Test
	public void Test2GetTrack() {
		// asserts for doubles and arrays?
		given().auth().preemptive().basic(user_name, user_password).spec(requestSpecBuilder.build()).log().all().when().get("/tracks/" + track_name).then().statusCode(200).contentType(ContentType.JSON).body("track_name", is(track_name))
		// .body("track_creator", is(user_name))
				.body("track_description", is(track_description));
		// .body("track_keywords", arrayContaining("kw1","kw2","kw3"))
		// .body("track_length", closeTo(track_length,1));
	}

	@Test
	public void Test31UpdateTrack() {
		given().auth().preemptive().basic(user_name, user_password).spec(requestSpecBuilder.build()).body("{\n\"track_description\" : \"new_description\"\n}").log().all().when().put("/tracks/" + track_name).then().statusCode(200);
	}

	@Test
	public void Test32GetUpdatedTrack() {
		given().auth().preemptive().basic(user_name, user_password).spec(requestSpecBuilder.build()).log().all().when().get("/tracks/" + track_name).then().statusCode(200).contentType(ContentType.JSON).body("track_description", is("new_description"));
	}

	@Test
	public void Test33UpdateTrackUserIsNotCreator() {
		given().auth().preemptive().basic(wrong_user_name, wrong_user_password).spec(requestSpecBuilder.build())
				.body("{\n\"track_name\" : \"" + track_name + "\",\n\"track_description\" : \"" + track_description + "\",\n\"track_keywords\" : " + JSON.objectToString(track_keywords) + ",\n\"track_geojson\" : \"" + track_geojson + "}").log().all().when().put("/tracks/" + track_name).then()
				.statusCode(403);
	}

	@Test
	public void Test4QueryTrack() {
		given().auth().preemptive().basic(user_name, user_password).spec(requestSpecBuilder.build()).log().all().queryParam("matches_name", track_name).when().get("/tracks").then().statusCode(200).contentType(ContentType.JSON).body("track_name", is(Arrays.asList(track_name)));
	}

	@Test
	public void Test51DeleteTrackUserIsNotCreator() {
		given().auth().preemptive().basic(wrong_user_name, wrong_user_password).spec(requestSpecBuilder.build()).log().all().when().delete("/tracks/" + track_name).then().statusCode(403);
	}

	@Test
	public void Test52DeleteTrack() {
		given().auth().preemptive().basic(user_name, user_password).spec(requestSpecBuilder.build()).log().all().when().delete("/tracks/" + track_name).then().statusCode(204);
	}
}
