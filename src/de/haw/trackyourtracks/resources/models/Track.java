package de.haw.trackyourtracks.resources.models;

import java.util.ArrayList;

import org.lightcouch.Document;

import com.google.gson.JsonElement;

public class Track extends Document {

	private String track_name;
	private String track_creator;
	private String track_description;
	private ArrayList<String> track_keywords;
	private JsonElement track_geojson;

	public Track(String track_name, String creator, String track_description, ArrayList<String> track_keywords, JsonElement track_geojson) {
		super();
		this.track_name = track_name;
		this.track_creator = creator;
		this.track_description = track_description;
		this.track_keywords = track_keywords;
		this.track_geojson = track_geojson;
	}

	public void setCreator(String track_creator) {
		this.track_creator = track_creator;
	}

	public String getName() {
		return track_name;
	}

	public void setName(String track_name) {
		this.track_name = track_name;
	}

	public String getDescription() {
		return track_description;
	}

	public void setDescription(String track_description) {
		this.track_description = track_description;
	}

	public ArrayList<String> getKeywords() {
		return track_keywords;
	}

	public void setKeywords(ArrayList<String> track_keywords) {
		this.track_keywords = track_keywords;
	}

	public JsonElement getGeojson() {
		return track_geojson;
	}

	public void setGeojson(JsonElement track_geojson) {
		this.track_geojson = track_geojson;
	}

	public String getCreator() {
		return track_creator;
	}

	public boolean isPopulated() {
		return track_name != null && track_description != null && track_keywords != null && track_geojson != null;
	}

}
