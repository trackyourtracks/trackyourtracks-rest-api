package de.haw.trackyourtracks.resources.models;

import java.util.HashMap;

import org.joda.time.LocalDate;
import org.lightcouch.Document;

public class Community extends Document {

	private String community_name;
	private String user_name;
	private String user_password;
    private boolean pub;
    private int users;
    private HashMap<String,Integer> api_requests = new HashMap<String,Integer>();
	
	public Community(String community_name, String user_name, String user_password, boolean pub) {
		super();
		this.community_name = community_name;
		this.user_name = user_name;
		this.user_password = user_password;
		this.pub = pub;
	}

	public void add_Api_Request(LocalDate date){
		api_requests.put(date.toString(), api_requests.get(date)+1 );
	}
	
	
	public HashMap<String,Integer> getApiRequests(){
		return this.api_requests;
	}
	
	public void initApiRequests(){
		this.api_requests = new HashMap<String,Integer>();
	}
	
	
	public void setUserPassword(String user_password) {
		this.user_password = user_password;
	}

	
	public boolean isPub(){
		return pub;
	}

	public void setCommunityName(String community_name) {
		this.community_name = community_name;
	}
	
	public void setUserName(String user_name){
		this.user_name = user_name;
	}

	public String getCommunityName() {
		return community_name;
	}

	public String getUserName() {
		return user_name;
	}

	public String getUserPassword() {
		return user_password;
	}
	
	public boolean isPopulated(){
		return community_name != null && user_name != null && user_password != null;
	}

	public int getUser() {
		return users;
	}

	public void incrementUser() {
		this.users++;
	}
	
	public void decrementUser() {
		this.users++;
	}

}
