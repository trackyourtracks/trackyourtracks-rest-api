package de.haw.trackyourtracks.resources.models;

import java.util.HashSet;

import com.cloudant.client.api.model.Document;

public class User extends Document {

	private String user_name;
	private String user_mail;
	private String user_password;
	private HashSet<String> user_track_ids = new HashSet<String>();

	public User(String user_name, String user_mail, String user_password) {
		super();
		this.user_name = user_name;
		this.user_mail = user_mail;
		this.user_password = user_password;
	}

	public String getName() {
		return user_name;
	}

	public String getMail() {
		return user_mail;
	}

	public String getPassword() {
		return user_password;
	}

	public boolean isPopulated() {
		return user_name != null && user_mail != null && user_password != null;
	}
	
	public void setPassword(String user_password){
		this.user_password=user_password;
	}
	
	public void addTrackID(String id){
		this.user_track_ids.add(id);
	}

	public void setName(String user_name) {
		this.user_name = user_name;
	}

	public void setMail(String user_mail) {
		this.user_mail = user_mail;
	}

	public void toLowerCase(){
		this.user_name = this.user_name.toLowerCase();
		this.user_mail = this.user_mail.toLowerCase();
	}
	
	public void initTrackIDs(){
		this.user_track_ids = new HashSet<String>();
	}

	public void deleteID(String id){
		this.user_track_ids.remove(id);	
	}
	
	public HashSet<String> getTrackIds() {
		return user_track_ids;
	}
	
	
}