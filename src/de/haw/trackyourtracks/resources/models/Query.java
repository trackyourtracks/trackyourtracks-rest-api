package de.haw.trackyourtracks.resources.models;

public class Query {

	private String matches_name;
	private String length_greater_than;
	private String length_less_than;


	public Query(String matches_name, String length_greater_than, String length_less_than) {
		this.matches_name = matches_name;
		this.length_greater_than = length_greater_than;
		this.length_less_than = length_less_than;
	}

	public String getMatchesName() {
		return matches_name;
	}

	public String getLengthGreaterThan() {
		return length_greater_than;
	}

	public String getLengthLessThan() {
		return length_less_than;
	}
	
	public Boolean isNull(){
		return matches_name==null && length_greater_than==null &&length_less_than==null;
	}

}
