package de.haw.trackyourtracks.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;

import de.haw.trackyourtracks.responses.Ok;

@Path("/infos")
public class Infos {

	@GET
	public Response getInfo(@Context HttpHeaders headers) {

		JsonObject info = new JsonObject();
		info.addProperty("version", "BETA");

		return Ok.HTTP_200(info, headers);
	}

}
