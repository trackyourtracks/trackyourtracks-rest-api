package de.haw.trackyourtracks.resources.checks;

import java.util.List;

import javax.ws.rs.core.HttpHeaders;
import javax.xml.bind.DatatypeConverter;

public class Creds {

	private String name;
	private String password;

	private Creds(String name, String password) {
		this.name = name.toLowerCase();
		this.password = password;
	}

	public static Creds parse(HttpHeaders headers) {

		List<String> header = headers.getRequestHeader("Authorization");

		if (header == null || header.isEmpty())
			return null;

		String creds = header.get(0);

		if (!creds.startsWith("Basic "))
			return null;

		creds = creds.replaceFirst("Basic ", "");

		try {
			creds = new String(DatatypeConverter.parseBase64Binary(creds));
		} catch (Exception e) {
			return null;
		}

		String[] split = creds.split(":");

		if (split.length != 2)
			return null;

		return new Creds(split[0], split[1]);
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

}
