package de.haw.trackyourtracks.resources.checks;

import java.util.List;

import javax.ws.rs.core.HttpHeaders;

public abstract class Protocol {

	public static boolean isHttps(HttpHeaders headers) {

		// developer.ibm.com/answers/questions/16016/how-do-i-enforce-ssl-for-my-bluemix-application.html#answer-16030

		List<String> origin = headers.getRequestHeader("X-Forwarded-Proto");
		return origin.get(0).equals("https");
	}

}
