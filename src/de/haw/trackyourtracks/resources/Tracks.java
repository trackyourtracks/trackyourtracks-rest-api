package de.haw.trackyourtracks.resources;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;

import de.haw.trackyourtracks.database.Cloudant;
import de.haw.trackyourtracks.database.IDatabase;
import de.haw.trackyourtracks.json.JSON;
import de.haw.trackyourtracks.resources.checks.Creds;
import de.haw.trackyourtracks.resources.checks.Protocol;
import de.haw.trackyourtracks.resources.models.Query;
import de.haw.trackyourtracks.resources.models.Track;
import de.haw.trackyourtracks.responses.Error;
import de.haw.trackyourtracks.responses.Ok;
import de.haw.trackyourtracks.security.Tokens;

@Path("/tracks")
public class Tracks {

	private IDatabase database = Cloudant.DATABASE;

	@POST
	public Response createTrack(@HeaderParam("Token") String token, @Context HttpHeaders headers, String body) {

		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

	
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		Response auth = auth(headers, token);

		if (auth.getStatus() != 200)
			return auth;

		Creds creds = Creds.parse(headers);

		Track track = JSON.toObject(body, Track.class);
		
		if (track == null || !track.isPopulated())
			return Error.HTTP_400(headers);

		if (database.containsTrackName(track.getName(), Tokens.getCommunityNameFromToken(token))) {
			// Trackname already taken
			return Error.HTTP_409(true, "track_name_taken", headers);
		}

		database.createTrack(creds.getName(), track, Tokens.getCommunityNameFromToken(token));

		return Ok.HTTP_201(headers);
	}

	@PUT
	@Path("{track_name}")
	public Response updateTrack(@HeaderParam("Token") String token, @PathParam("track_name") String track_name, @Context HttpHeaders headers, String body) {

		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		
		Response auth = auth(headers, token);

		if (auth.getStatus() != 200)
			return auth;

		Track track = JSON.toObject(body, Track.class);

		// If the user is not the owner from the track
		if (!database.getTrack(track_name, Tokens.getCommunityNameFromToken(token)).getCreator().equals(database.getUser(Creds.parse(headers).getName(), Tokens.getCommunityNameFromToken(token)).getId()))
			return Error.HTTP_403(headers);
				
		// new trackname already taken
		if (database.containsTrackName(track.getName(),Tokens.getCommunityNameFromToken(token)) && !(track.getName().equals(track_name)))
				return Error.HTTP_409(true,"track_name_taken", headers);

		database.updateTrack(track_name, track, Tokens.getCommunityNameFromToken(token));
		return Ok.HTTP_200(headers);
	}

	@GET
	@Path("{track_name}")
	public Response getTrack(@HeaderParam("Token") String token, @PathParam("track_name") String track_name, @Context HttpHeaders headers) {

		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		
		Response auth = auth(headers, token);

		if (auth.getStatus() != 200)
			return auth;

		Track track = database.getTrack(track_name.toLowerCase(), Tokens.getCommunityNameFromToken(token));
		
		if(track == null)
			return Error.HTTP_404(headers);
		
		String user_id = database.getUser(Creds.parse(headers).getName(), Tokens.getCommunityNameFromToken(token)).getId();

		JsonObject json_track = JSON.objectToJsonObject(track);
		json_track.addProperty("is_track_creator", track.getCreator().equals(user_id));
				
		return Ok.HTTP_200(json_track, headers);
	}

	@DELETE
	@Path("{track_name}")
	public Response deleteTrack(@HeaderParam("Token") String token, @PathParam("track_name") String track_name, @Context HttpHeaders headers) {

		
		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		Response auth = auth(headers, token);

		if (auth.getStatus() != 200)
			return auth;
		
		
		//Track exist?
        Track trackCheck = database.getTrack(track_name.toLowerCase(), Tokens.getCommunityNameFromToken(token));
		if(trackCheck == null)
			return Error.HTTP_404(headers);

		// If the user is not the owner from the Track
		if (!database.getTrack(track_name.toLowerCase(), Tokens.getCommunityNameFromToken(token)).getCreator().equals(database.getUser(Creds.parse(headers).getName(), Tokens.getCommunityNameFromToken(token)).getId()))
			return Error.HTTP_403(headers);

		database.deleteTrack(track_name.toLowerCase(), Tokens.getCommunityNameFromToken(token));

		return Ok.HTTP_204(headers);
	}

	@GET
	public Response queryTracks(@HeaderParam("Token") String token, @QueryParam("matches_name") String matches_name, @QueryParam("length_greater_than") String length_greater_than, @QueryParam("length_less_than") String length_less_than, @Context HttpHeaders headers) {

		
		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		Response auth = auth(headers, token);

		if (auth.getStatus() != 200)
			return auth;

		Query query = new Query(matches_name, length_greater_than, length_less_than);

		List<Track> tracks = database.queryTracks(query, Tokens.getCommunityNameFromToken(token));

		return Ok.HTTP_200(tracks, headers);
	}

	private Response auth(HttpHeaders headers, String token) {

		if (!Protocol.isHttps(headers))
			return Error.HTTP_505(headers);

		Creds creds = Creds.parse(headers);

		if (creds == null)
			return Error.HTTP_401(headers);

		// if (!database.containsUserName(creds.getName()))
		// return Error.HTTP_404(headers);

		if (!database.authUser(creds, Tokens.getCommunityNameFromToken(token)))
			return Error.HTTP_401(headers);

		return Ok.HTTP_200(headers);
	}

	@OPTIONS
	public Response options(@Context HttpHeaders headers) {
		return Ok.HTTP_200(headers);
	}

	@OPTIONS
	@Path("{track_name}")
	public Response trackOptions(@Context HttpHeaders headers) {
		return Ok.HTTP_200(headers);
	}

}
