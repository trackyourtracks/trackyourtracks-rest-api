package de.haw.trackyourtracks.resources;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import de.haw.trackyourtracks.database.Cloudant;
import de.haw.trackyourtracks.database.IDatabase;
import de.haw.trackyourtracks.json.JSON;
import de.haw.trackyourtracks.resources.checks.Creds;
import de.haw.trackyourtracks.resources.checks.Protocol;
import de.haw.trackyourtracks.resources.models.Track;
import de.haw.trackyourtracks.resources.models.User;
import de.haw.trackyourtracks.responses.Error;
import de.haw.trackyourtracks.responses.Ok;
import de.haw.trackyourtracks.security.Tokens;

@Path("/users")
public class Users {

	private IDatabase database = Cloudant.DATABASE;

	@POST
	public Response createUser(@HeaderParam("Token") String token, @Context HttpHeaders headers, String body) {

		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));

		if (!Protocol.isHttps(headers))
			return Error.HTTP_505(headers);

		User user = JSON.toObject(body, User.class);

		if (user == null || !user.isPopulated() || user.getName() == null || user.getMail() == null || user.getPassword() == null)
			return Error.HTTP_400(headers);

		user.setName(user.getName().toLowerCase());
		user.setMail(user.getMail().toLowerCase());
		

		if (!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		if (!Tokens.ckeckPermission(token, Tokens.getCommunityNameFromToken(token)))
			return Error.HTTP_403(headers);

		if (database.containsUserName(user.getName(), Tokens.getCommunityNameFromToken(token))) {

			if (database.containsUserMail(user.getMail(), Tokens.getCommunityNameFromToken(token)))
				// user_name and user_mail taken
				return Error.HTTP_409(true, true, headers);

			// only user_name taken
			return Error.HTTP_409(true, false, headers);
		}

		if (database.containsUserMail(user.getMail(), Tokens.getCommunityNameFromToken(token)))
			// only user_mail taken
			return Error.HTTP_409(false, true, headers);
		
		
		
		//increment user counter
		database.incrementUserCounter(Tokens.getCommunityNameFromToken(token));

		database.createUser(user, Tokens.getCommunityNameFromToken(token));

		return Ok.HTTP_201(headers);
	}

	@PUT
	@Path("{user_name}")
	public Response updateUser(@HeaderParam("Token") String token, @PathParam("user_name") String user_name, @Context HttpHeaders headers, String body) {

		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

	
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		Response auth = auth(headers, user_name, token);

		if (auth.getStatus() != 200)
			return auth;

		User user = JSON.toObject(body, User.class);
		
		if(user.getName()!= null)
		user.setName(user.getName().toLowerCase());

		if (user == null)
			return Error.HTTP_400(headers);

		
		if (database.containsUserName(user.getName(), Tokens.getCommunityNameFromToken(token)) && !user.getName().equals(user_name) ) {

			if (database.containsUserMail(user.getMail(), Tokens.getCommunityNameFromToken(token)))
				// user_name and user_mail taken
				return Error.HTTP_409(true, true, headers);

			// only user_name taken
			return Error.HTTP_409(true, false, headers);
		}

		if (database.containsUserMail(user.getMail(), Tokens.getCommunityNameFromToken(token)))
			// only user_mail taken
			return Error.HTTP_409(false, true, headers);

		database.updateUser(user_name.toLowerCase(), user, Tokens.getCommunityNameFromToken(token));

		return Ok.HTTP_200(headers);
	}

	@GET
	@Path("{user_name}")
	public Response getUser(@HeaderParam("Token") String token, @PathParam("user_name") String user_name, @Context HttpHeaders headers) {

		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		Response auth = auth(headers, user_name.toLowerCase(), token);

		if (auth.getStatus() != 200)
			return auth;

		User user = database.getUser(user_name.toLowerCase(), Tokens.getCommunityNameFromToken(token));

		return Ok.HTTP_200(user, headers);
	}

	@DELETE
	@Path("{user_name}")
	public Response deleteUser(@HeaderParam("Token") String token, @PathParam("user_name") String user_name, @Context HttpHeaders headers) {

		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		Response auth = auth(headers, user_name, token);

		if (auth.getStatus() != 200)
			return auth;

		//Decrement User Counter
		database.DecrementUserCounter(Tokens.getCommunityNameFromToken(token));
		
		database.deleteUser(user_name.toLowerCase(), Tokens.getCommunityNameFromToken(token));

		return Ok.HTTP_204(headers);
	}

	@GET
	@Path("{user_name}/tracks")
	public Response getTracks(@HeaderParam("Token") String token, @PathParam("user_name") String user_name, @Context HttpHeaders headers) {

		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		Response auth = auth(headers, user_name, token);

		if (auth.getStatus() != 200)
			return auth;

		List<Track> tracks = database.getTracks(user_name.toLowerCase(), Tokens.getCommunityNameFromToken(token));

		return Ok.HTTP_200(tracks, headers);
	}

	@POST
	@Path("{user_name}/tracks/{track_name}")
	public Response addTrack(@HeaderParam("Token") String token, @PathParam("user_name") String user_name, @PathParam("track_name") String track_name, @Context HttpHeaders headers) {

		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		
		Response auth = auth(headers, user_name, token);

		if (auth.getStatus() != 200)
			return auth;

		database.addTrack(user_name.toLowerCase(), track_name.toLowerCase(), Tokens.getCommunityNameFromToken(token));

		return Ok.HTTP_201(headers);
	}

	@DELETE
	@Path("{user_name}/tracks/{track_name}")
	public Response removeTrack(@HeaderParam("Token") String token, @PathParam("user_name") String user_name, @PathParam("track_name") String track_name, @Context HttpHeaders headers) {

		if(!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		
		//increment api requests
		database.addApiRequest(Tokens.getCommunityNameFromToken(token));
		
		
		Response auth = auth(headers, user_name, token);

		if (auth.getStatus() != 200)
			return auth;

		database.removeTrack(user_name, track_name.toLowerCase(), Tokens.getCommunityNameFromToken(token));

		return Ok.HTTP_204(headers);
	}

	private Response auth(HttpHeaders headers, String user_name, String token) {

		if (!Protocol.isHttps(headers))
			return Error.HTTP_505(headers);

		Creds creds = Creds.parse(headers);
		
		if (creds == null)
			return Error.HTTP_401(headers);

		if (!user_name.toLowerCase().equals(creds.getName()))
			return Error.HTTP_403(headers);

		if (token == null) // Hier anderen Fehlerreturncode da ja ein Token
							// Fehler?
			return Error.HTTP_401(headers);

		if (!Tokens.validateToken(token))
			return Error.HTTP_401(headers);

		if (!Tokens.ckeckPermission(token, Tokens.getCommunityNameFromToken(token)))
			return Error.HTTP_403(headers);

		if (!database.authUser(creds, Tokens.getCommunityNameFromToken(token)))
			return Error.HTTP_401(headers);

		return Ok.HTTP_200(headers);
	}

	@OPTIONS
	public Response options(@Context HttpHeaders headers) {
		return Ok.HTTP_200(headers);
	}

	@OPTIONS
	@Path("{user_name}")
	public Response userOptions(@Context HttpHeaders headers) {
		return Ok.HTTP_200(headers);
	}

	@OPTIONS
	@Path("{user_name}/tracks")
	public Response userTracksOptions(@Context HttpHeaders headers) {
		return Ok.HTTP_200(headers);
	}

	@OPTIONS
	@Path("{user_name}/tracks/{track_name}")
	public Response userTracksTrackOptions(@Context HttpHeaders headers) {
		return Ok.HTTP_200(headers);
	}

}
