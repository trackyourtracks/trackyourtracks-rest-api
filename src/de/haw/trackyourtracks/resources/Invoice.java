package de.haw.trackyourtracks.resources;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.cloudant.client.api.Database;
import com.google.gson.JsonObject;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import de.haw.trackyourtracks.database.Cloudant;
import de.haw.trackyourtracks.database.IDatabase;
import de.haw.trackyourtracks.responses.Ok;
import de.haw.trackyourtracks.security.Tokens;


@Path("/invoice")
public class Invoice {
	
	
	
	@POST
	public Response sendInvoice(@HeaderParam("com") String com,@HeaderParam("mail") String mail, @Context HttpHeaders headers) {

		JsonObject info = new JsonObject();
		info.addProperty("Invoice send", "true");
	
		generateBill(com);
		sendBill(mail);
	
		return Ok.HTTP_200(info, headers);
	}
	
	
	@GET
	public Response getInvoice(@HeaderParam("com") String com, @Context HttpHeaders headers) {
		
		IDatabase database = Cloudant.DATABASE;
		

		JsonObject invoice = new JsonObject();
		DecimalFormat f = new DecimalFormat("#0.00");
		
	    invoice.addProperty("count", database.getCommunityUserCounter(com));
	    
		double netto = database.getCommunityUserCounter(com)*0.039;
		invoice.addProperty("netto", f.format(netto));
		
		double mehrwert = netto/100*19;
		invoice.addProperty("mehrwert", f.format(mehrwert));
	    
		double brutto= netto + mehrwert;
		invoice.addProperty("brutto", f.format(brutto));
		

		return Ok.HTTP_200(invoice, headers);
	}
	
	
	@OPTIONS
	public Response options(@Context HttpHeaders headers) {
		return Ok.HTTP_200(headers);
	}
	
	
	
	
	
	  private static void sendBill(String to) {
			
	        // Sender's email ID needs to be mentioned
	        String from = "trackyourtracks@gmx.de";

	        final String username = "trackyourtracks@gmx.de";//change accordingly
	        final String password = "123456789";//change accordingly

	        // Assuming you are sending email through mail.gmx.net
	        String host = "mail.gmx.net";

	        Properties props = new Properties();
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.host", host);
	        props.put("mail.smtp.port", "587");

	        // Get the Session object.
	        Session session = Session.getInstance(props,
	           new javax.mail.Authenticator() {
	              protected PasswordAuthentication getPasswordAuthentication() {
	                 return new PasswordAuthentication(username, password);
	              }
	           });

	        try {
	           // Create a default MimeMessage object.
	           Message message = new MimeMessage(session);

	           // Set From: header field of the header.
	           message.setFrom(new InternetAddress(from));

	           // Set To: header field of the header.
	           message.setRecipients(Message.RecipientType.TO,
	              InternetAddress.parse(to));

	           //Get current month&year
	           GregorianCalendar cal = new GregorianCalendar();
	           int year = cal.get(Calendar.YEAR);
	           String month = new SimpleDateFormat("MMMM", Locale.GERMAN).format(cal.getTime());
	           
	           // Set Subject: header field
	           message.setSubject("TrackYourTracks Rechung "+month+" "+year );

	           // Create the message part
	           BodyPart messageBodyPart = new MimeBodyPart();

	           // Now set the actual message
	           messageBodyPart.setText("Sehr geehrte Damen und Herren,\n\nim Anhang schicken wir Ihnen hiermit die Rechung f�r den Monat "+month+".\nVielen Dank, dass Sie unseren Service weiterhin in Anspruch nehmen.\n\n\n\nMit freundlichen Gr��en\nIhr TrackYourTracks Team");

	           // Create a multipar message
	           Multipart multipart = new MimeMultipart();

	           // Set text message part
	           multipart.addBodyPart(messageBodyPart);

	           // Part two is attachment
	           messageBodyPart = new MimeBodyPart();
	           String filename = "Invoice.pdf";
	           DataSource source = new FileDataSource(filename);
	           messageBodyPart.setDataHandler(new DataHandler(source));
	           messageBodyPart.setFileName(filename);
	           multipart.addBodyPart(messageBodyPart);

	           // Send the complete message parts
	           message.setContent(multipart);

	           // Send message
	           Transport.send(message);

	           System.out.println("Sent message successfully....");
	    
	        } catch (MessagingException e) {
	           throw new RuntimeException(e);
	        }
	  
	
}



private static void generateBill(String com){
	IDatabase database = Cloudant.DATABASE;
	
	
	  try {
	      PdfReader pdfReader = new PdfReader("Vorlage.pdf");

	      PdfStamper pdfStamper = new PdfStamper(pdfReader,
	            new FileOutputStream("Invoice.pdf"));

	      
	      PdfContentByte content = pdfStamper.getOverContent(1);
	      
	      DecimalFormat f = new DecimalFormat("#0.00");
	      
	      Date date = new Date();
	      SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
	      
	      ColumnText.showTextAligned(content, Element.ALIGN_LEFT, new Phrase(sdf.format(date).toString()), 460, 620, 0);
	      ColumnText.showTextAligned(content, Element.ALIGN_LEFT, new Phrase(String.valueOf(database.getCommunityUserCounter(com))), 322, 432, 0);
	      double netto = database.getCommunityUserCounter(com)*0.039;
	      ColumnText.showTextAligned(content, Element.ALIGN_LEFT, new Phrase(f.format(netto)), 485, 432, 0);
	      ColumnText.showTextAligned(content, Element.ALIGN_LEFT, new Phrase(f.format(netto)), 440, 375, 0);
	      double mehrwert = netto/100*19;
	      double brutto= netto + mehrwert;
	      ColumnText.showTextAligned(content, Element.ALIGN_LEFT, new Phrase(f.format(mehrwert)), 463, 360, 0);
	      ColumnText.showTextAligned(content, Element.ALIGN_LEFT, new Phrase(f.format(brutto)), 463, 347, 0);
	      
	      
	      pdfStamper.close();

	    } catch (IOException e) {
	      e.printStackTrace();
	    } catch (DocumentException e) {
	      e.printStackTrace();
	    }
}
	
	

}




